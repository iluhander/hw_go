package main

import (
	"gitlab.com/iluhander/hw_go/hw_2/client/config"
	"gitlab.com/iluhander/hw_go/hw_2/client/test"
)

func main() {
	testsConfig := config.Config{TargetHost: "127.0.0.1", TargetPort: "3333"}
	test.RunTests(testsConfig)
}
