package test

import (
	"fmt"
	"hash/fnv"
	"math/rand"
	"time"

	"gitlab.com/iluhander/hw_go/hw_1/pkg"
)

type Generator0 struct{}

func (g Generator0) Generate(name string) (bool, pkg.BookId) {
	var val uint = 0
	for _, symb := range name {
		val += uint(symb) * 100
	}

	return true, pkg.BookId(val)
}

type Generator1 struct{}

func (g Generator1) Generate(name string) (bool, pkg.BookId) {
	hash := fnv.New64a()
	hash.Write([]byte(name))

	val := hash.Sum64()

	return true, pkg.BookId(val)
}

var letters = []rune("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ")

func makeRandomStr(length int) string {
	bytes := make([]rune, length)

	for i := range bytes {
		bytes[i] = letters[rand.Intn(len(letters))]
	}

	return string(bytes)
}

func fillLib(lib pkg.Library) []pkg.Book {
	rand.Seed(time.Now().UnixNano())

	books := make([]pkg.Book, 0, 5)

	fmt.Println("  Generated and added to the lib:")
	for i := 0; i < 5; i++ {
		newBook := pkg.Book{makeRandomStr(10), makeRandomStr(10)}
		msg := fmt.Sprintf("    Book #%d: name - '%s', author - '%s'", i, newBook.Name, newBook.Author)
		fmt.Println(msg)

		books = append(books, newBook)

		lib.AddBook(newBook)
	}

	return books
}

func testAddToLib(lib pkg.Library, books []pkg.Book) {
	fmt.Println("\n  Adding tests:")
	for i := 0; i < 2; i++ {
		ok, bookFound := lib.GetBookByName(books[i].Name)

		if !ok {
			msg := fmt.Sprintf("    Test #%d failed due to error", i)
			fmt.Println(msg)
		} else if bookFound.Author != books[i].Author {
			msg := fmt.Sprintf("    Test for book #%d failed: expected '%s', got '%s'", i, books[i].Author, bookFound.Author)
			fmt.Println(msg)
		} else {
			msg := fmt.Sprintf("    Test for book #%d succeeded: expected '%s', got '%s'", i, books[i].Author, bookFound.Author)
			fmt.Println(msg)
		}
	}
}

func testMigrateLib(lib0 pkg.Library, lib1 pkg.Library, books []pkg.Book) {
	pkg.MigrateStore(&lib0, &lib1)

	fmt.Println("  Migration tests:")
	for i := 0; i < 5; i++ {
		ok, lib0BookFound := lib0.GetBookByName(books[i].Name)
		ok, lib1BookFound := lib1.GetBookByName(books[i].Name)

		if !ok {
			msg := fmt.Sprintf("    Test #%d failed due to error", i)
			fmt.Println(msg)
		} else if lib0BookFound.Author != lib1BookFound.Author {
			msg := fmt.Sprintf("    Test for book #%d failed: expected '%s', got '%s'", i, lib0BookFound.Author, lib1BookFound.Author)
			fmt.Println(msg)
		} else {
			msg := fmt.Sprintf("    Test for book #%d succeeded: expected '%s', got '%s'", i, lib0BookFound.Author, lib1BookFound.Author)
			fmt.Println(msg)
		}
	}
}

func RunTests() {
	// С первым генератором и первым хранилищем.
	mapLib := pkg.MapLibrary{}
	g0 := pkg.BookIdGenerator(Generator0{})
	mapLib.Init(&g0)

	fmt.Println("Testing map implementation:")
	books := fillLib(&mapLib)
	testAddToLib(&mapLib, books)

	fmt.Println()
	// Со вторым генератором и вторым хранилищем.
	sliceLib := pkg.SliceLibrary{}
	g1 := pkg.BookIdGenerator(Generator1{})
	sliceLib.Init(&g1)

	fmt.Println("Testing slice implementation:")
	testMigrateLib(&mapLib, &sliceLib, books)
}
