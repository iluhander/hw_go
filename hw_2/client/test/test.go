package test

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/iluhander/hw_go/hw_2/client/config"
)

func printReponse(loggingPrefix, endpoint, resBody string, resCode int) {
	fmt.Printf("%sFor endpoint: %s\n", loggingPrefix, endpoint)
	fmt.Printf("%sGot response: %s\n", loggingPrefix, resBody)
	fmt.Printf("%sStatus code: %d\n", loggingPrefix, resCode)
}

func testGetVersion(requestURL string, loggingPrefix string) {
	endpoint := fmt.Sprintf("%s%s", requestURL, "/version")

	res, err := http.Get(endpoint)
	if err != nil {
		fmt.Printf("%sError: %s\n", loggingPrefix, err)
		return
	}

	// Reading and loggind.
	resBody, err := io.ReadAll(res.Body)

	printReponse(loggingPrefix, endpoint, string(resBody), res.StatusCode)
}

func testDecodeStr(requestURL string, loggingPrefix string) {
	endpoint := fmt.Sprintf("%s%s", requestURL, "/decode")

	// Encoding.
	inputStr := base64.StdEncoding.EncodeToString([]byte("yyy"))
	jsonBody := []byte(fmt.Sprintf(`{"inputString": "%s"}`, inputStr))
	bodyReader := bytes.NewReader(jsonBody)

	// Making the request.
	req, err := http.NewRequest(http.MethodPost, endpoint, bodyReader)
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{}

	res, err := client.Do(req)

	if err != nil {
		fmt.Printf("%sError: %s\n", loggingPrefix, err)
		return
	}

	// Decoding and loggind.
	resBody, err := io.ReadAll(res.Body)

	printReponse(loggingPrefix, endpoint, string(resBody), res.StatusCode)
}

func testHardOp(requestURL string, loggingPrefix string) {
	endpoint := fmt.Sprintf("%s%s", requestURL, "/hard-op")

	bodyReader := bytes.NewReader([]byte(""))
	req, err := http.NewRequest(http.MethodGet, endpoint, bodyReader)
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{
		Timeout: 15 * time.Second,
	}

	res, err := client.Do(req)

	if err != nil {
		fmt.Printf("%sError: %s\n", loggingPrefix, err)
		return
	}

	// Reading and loggind.
	resBody, err := io.ReadAll(res.Body)

	printReponse(loggingPrefix, endpoint, string(resBody), res.StatusCode)
}

func RunTests(config config.Config) {
	requestURL := fmt.Sprintf("http://%s:%s", config.TargetHost, config.TargetPort)
	loggingPrefix := "    "

	println("Running tests: \n")

	println("  Test #0:")
	testGetVersion(requestURL, loggingPrefix)

	println("\n  Test #1:")
	testDecodeStr(requestURL, loggingPrefix)

	println("\n  Test #2:")
	testHardOp(requestURL, loggingPrefix)

	println("\nDone")
}
