package pkg

type BookId uint64

type Book struct {
	Name   string
	Author string
}

type BookIdGenerator interface {
	Generate(name string) (bool, BookId)
}

type Library interface {
	AddBook(book Book) bool
	GetBookByName(bookNamse string) (bool, Book)
	Init(b *BookIdGenerator)
}

type baseLibrary struct {
	generator *BookIdGenerator
}
