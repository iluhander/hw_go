package config

type Config struct {
	TargetHost string
	TargetPort string
}
