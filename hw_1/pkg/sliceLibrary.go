package pkg

type SliceLibrary struct {
	store []Book

	/*
		На случай, если вычисление BookId займет много времени,
		идентификаторы книг сохраняются в данный слайс.

		По нему затем будем происходить поиск (при вызове getBook).
	*/
	ids []BookId

	baseLibrary
}

func (lib *SliceLibrary) Init(generator *BookIdGenerator) {
	lib.store = make([]Book, 0, 0)

	lib.ids = make([]BookId, 0, 0)
	lib.generator = generator
}

func (lib *SliceLibrary) AddBook(book Book) bool {
	ok, id := (*lib.generator).Generate(book.Name)

	if !ok {
		return false
	}

	lib.store = append(lib.store, book)
	lib.ids = append(lib.ids, id)

	return true
}

func (lib *SliceLibrary) GetBookByName(name string) (bool, Book) {
	ok, targetId := (*lib.generator).Generate(name)

	if !ok {
		return false, Book{}
	}

	for i, curId := range lib.ids {
		if curId == targetId {
			return true, lib.store[i]
		}
	}

	return true, Book{}
}
