package pkg

type MapLibrary struct {
	store map[BookId]Book
	baseLibrary
}

func (lib *MapLibrary) Init(generator *BookIdGenerator) {
	lib.store = make(map[BookId]Book)
	lib.generator = generator
}

func (lib *MapLibrary) AddBook(book Book) bool {
	ok, id := (*lib.generator).Generate(book.Name)

	if !ok {
		return false
	}

	lib.store[id] = book

	return true
}

func (lib *MapLibrary) GetBookByName(bookName string) (bool, Book) {
	ok, id := (*lib.generator).Generate(bookName)

	if !ok {
		return false, Book{}
	}

	return true, lib.store[id]
}
