package internal

import (
	"net/http"
)

type PathMethods struct {
	methods []string
}

func (pathMethods *PathMethods) addMethod(method string) {
	pathMethods.methods = append(pathMethods.methods, method)
}

func (pathMethods *PathMethods) hasMethod(method string) bool {
	for _, v := range pathMethods.methods {
		if v == method {
			return true
		}
	}

	return false
}

type Router struct {
	mux    *http.ServeMux
	routes map[string]*PathMethods
}

func (router *Router) Route(method, path string, handler func(http.ResponseWriter, *http.Request)) {
	if router.routes[path] == nil {
		router.routes[path].methods = make([]string, 0, 10)
	}

	router.routes[path].addMethod(method)

	router.mux.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		if router.routes[path].hasMethod(method) {
			handler(w, r)
		}

		http.Error(w, "Method not allowed", http.StatusInternalServerError)
	})
}
