package pkg

func MigrateStore(lib0 *Library, lib1 *Library) {
	if mapLib, ok := (*lib0).(*MapLibrary); ok {
		for _, book := range mapLib.store {
			(*lib1).AddBook(book)
		}
	}

	if sliceLib, ok := (*lib0).(*SliceLibrary); ok {
		for _, book := range sliceLib.store {
			(*lib1).AddBook(book)
		}
	}
}
