package app

import (
	"context"
	"fmt"
	"net/http"
)

type App struct {
	server *http.Server
}

func (app *App) Init() {
}

func (app *App) Run() {
	go func() {
		err := app.server.ListenAndServe()
		if err != nil {
			fmt.Println("Error in app: ", err)
		}
	}()
}

func (app *App) Stop(ctx context.Context) {
	fmt.Println(app.server.Shutdown(ctx))
}
